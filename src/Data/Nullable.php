<?php

$exports["null"] = NULL;

$exports['nullable'] = function ($a, $r, $f) {
  return $a == NULL ? $r : $f($a);
};

$exports['notNull'] = function ($x) {
  return x;
};
